import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class AddBookFraction {
	public static void main(String[] args) {
		final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
		final String JDBC_URL = "jdbc:mysql://localhost:3306/book_rental_db";
		final String JDBC_USER = "root";
		final String JDBC_PWD = "root";
		
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			Class.forName(DRIVER_CLASS);
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
			return;
		}
		
		try(Connection con = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PWD)){
			Statement st = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			
			System.out.println("         Give New Book Details");
			System.out.println("        =======================");
			
			System.out.print("Title: ");
			String title = input.readLine();
			
			System.out.print("Y.O.P.: ");
			int yop = Integer.parseInt(input.readLine());
			
			System.out.print("Edition: ");
			int edition = Integer.parseInt(input.readLine());
			
			System.out.print("Price: ");
			double price = Double.parseDouble(input.readLine());
			
			System.out.println("N.O.P.: ");
			int numberOfPages = Integer.parseInt(input.readLine());
			
			System.out.print("I.S.B.N.: ");
			String isbn = input.readLine();
			
			System.out.print("Quantity: ");
			int quantity = Integer.parseInt(input.readLine());
			
			System.out.print("Publisher (ID): ");
			int publisherId = Integer.parseInt(input.readLine());
			
			List<Integer> authors = new LinkedList();
			
			System.out.println("Authors (ID's): ");
			String authorIdString = input.readLine();
			
			StringTokenizer tokens = new StringTokenizer(authorIdString);
			
			while(tokens.hasMoreTokens()) {
				authors.add(Integer.parseInt(tokens.nextToken()));
			}
			
			con.setAutoCommit(false);
			
			pst = con.prepareStatement("INSERT INTO books VALUES(0, ?, ?, ?, ?, ?, ?, ?)");
			pst.setString(1, title);
			pst.setDouble(3, price);
			pst.setInt(2, publisherId);
			pst.setInt(4, yop);
			pst.setInt(5, edition);
			pst.setInt(6, numberOfPages);
			pst.setString(7, isbn);			
			
			pst.executeUpdate();
			
			rs = con.createStatement().executeQuery("SELECT book_id FROM books WHERE title='" + title + "' "
					+ "ORDER BY book_id DESC LIMIT 1");
			int bookId = 0;
			if(rs.next()) {
				bookId = rs.getInt(1);
			}
			
			pst = con.prepareStatement("INSERT INTO books_stock VALUES(0, ?, ?, ?, ?)");

			pst.setInt(1, bookId);
			pst.setDouble(2, price);
			pst.setDouble(3, price / 100);
			pst.setInt(4, 1);
			
			for(int i=0; i<quantity; i++) {
				pst.executeUpdate();
			}
			
			pst = con.prepareStatement("INSERT INTO book_authors VALUES(0, ?, ?)");
			pst.setInt(1, bookId);
			
			for(Integer authorId: authors) {
				pst.setInt(2, authorId);
				pst.executeUpdate();
			}
			
			con.commit();
			
			System.out.println("    BOOK ADDED SUCCESSFULLY");
			
			con.setAutoCommit(true);
		}catch(SQLException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
